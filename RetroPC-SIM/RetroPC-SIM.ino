/* RETRO PC - SIMULATOR  --------------------------------------------------------
 * Developed by Luis Quijada
 * jose.jlq@hotmail.com
 * Nov 2018
 * 
 * Oficial Website: https://retropc-sim.blogspot.com/
 * Repository: https://bitbucket.org/elezeroteam/retropc-sim/src/master/
 * 
 * 
 * Features:
 *    Registry EAX, EBX, ECX, EDX and FLAGS
 *    Operation with registrires
 *    Arithmetic/Loic Operations
 *    Pin input/output Operations
 *    Negative numbers (Complement to 2)
 *    
 * ------------------------------------------------------------------------------
 */



//Constants CardReader Pins
const byte pcm_co = 13;        //Perfored Card Reader Machine - continuity bit
const byte pcm_reader = 12;    //Perfored Card Reader Machine - reader bit
const byte pcm_button = 11;    //Perfored Card Reader Machine - machine mode button
const byte ledPin = 2;

//Execution variables
int AX = 0;       //AX registry
int BX = 0;       //BX registry
int LRESULT = 0;  //Result of last operation



// Registry   ------------------------------------------------------
//------------------   General prupose registry ----------------------
const byte genreg_length = 32;

//                           ***********************************************************************************************
//                       EAX *                                              |                      AX                      *   // 16 bits
//                           *                                              |          AH           |           AL         *   //  8 bits c/u
boolean EAX[genreg_length] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}; // 32 bits
//    INDEXES                 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31

//                           ***********************************************************************************************
//                       EBX *                                              |                      BX                      *   // 16 bits
//                           *                                              |          BH           |           BL         *   //  8 bits c/u
boolean EBX[genreg_length] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}; // 32 bits
//    INDEXES                 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31

//                           ***********************************************************************************************
//                       ECX *                                              |                      CX                      *   // 16 bits
//                           *                                              |          CH           |           CL         *   //  8 bits c/u
boolean ECX[genreg_length] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}; // 32 bits
//    INDEXES                 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31

//                           ***********************************************************************************************
//                       EDX *                                              |                      DX                      *   // 16 bits
//                           *                                              |          DH           |           DL         *   //  8 bits c/u
boolean EDX[genreg_length] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}; // 32 bits
//    INDEXES                 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31


// --------------- Segment Registry ------------------
//const byte segreg_length = 16;

//boolean CS[segreg_length] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0};
//boolean DS[segreg_length] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0};
//boolean SS[segreg_length] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0};
//boolean ES[segreg_length] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0};


// --------------- FLAGS Registry ------------------
const byte flagreg_length = 16;
//                               CF 1 PF  0  AF 0 ZF SF TF IF DF OF  -  -  -  - 
boolean FLAGS[flagreg_length] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0};
//                               0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15



//Control variables
// +++++++++++++++ CARDREADER MODE ++++++++++++++++++
// CardReader control variables
boolean status_pcm_co;        // status of machine_continuity reader sensor (0: no reading, 1:reading)
boolean status_pcm_reader;      //status of machine_bit reader sensor (0,1)
boolean lstatus_pcm_co = 1;     //last co state
boolean lstatus_pcm_reader;   //last reader state
boolean status_pcm_button;    //status of pcm_button
boolean mmode = 0;          //machine mode (0:waiting, 1:reading)
int approbs = 0;        //approbedd status 
int cocounter = 0;        // co status on counter
byte co_margen = 1000;       // co status on margen (allow to read from reader when reads 100 co)
unsigned int appro1 = 0;         //certain 1 counter
unsigned int appro0 = 0;         //certain 0 counter
unsigned int appron = 0;         //certain counter
int statusxx = 0;               //calculated status
byte abbio = 0;                 //argument bits by operation read
String instrucname = "";        //name of instruction read
// +++++++++++++++ GENERAL CONTROL VARIABLES +++++++
bool inputError = 0;      //false: input contains correct binary format, true: otherwise
bool showPrompt = 1;      //1: show ">" on screen, 0: not show it
const byte nmaxi = 16;   // number of instructions declared
byte aiclength = 4;      //arguments and intruction lenght(3 for args, 4 for instruction code)
String instruction;      //final instruction   String (Binary String)
int ninstruction;        // instruction number read (decimal)
String arguments;        //final instructions arguments (bits)
bool imode = 0;          // input mode (0: Serial input, 1: CardReader input)

// *************************************************************
//        CODES OF INSTRUCTIONS AND ARGS LENGTH DEFINITIONS
const byte SETPINMODE  =    0;      // 0000
const byte SETPINSTATUS =   1;      // 0001
const byte ADDITION =       2;      // 0010
const byte SUBSTRACTION =   3;      // 0011
const byte MULTIPLICATION = 4;      // 0100
const byte DIVISION =       5;      // 0101
const byte DISPLAYLASTRESULT = 6;   // 0110
const byte SETAXVAL =       7;      // 0111
const byte SETBXVAL =       8;      // 1000
const byte SHOWREG =        9;      // 1001
const byte SHOWREGS =       10;     // 1010
const byte SETREGVALUE =    11;     // 1011
const byte GETREGVALUE =    12;     // 1100
const byte COMPARE =        13;     // 1101
const byte READPINSTATUS =  14;     // 1110
const byte CHANGEMODE =     15;     // 1111

//Array with # of arguments by each instruction 
byte abbix[nmaxi] = { 6,   // 0000
                      5,   // 0001 
                      0,   // 0010
                      0,   // 0011
                      0,   // 0100
                      0,   // 0101
                      0,   // 0110
                      6,   // 0111
                      6,   // 1000
                      3,   // 1001
                      0,   // 1010
                      35,  // 1011
                      3,   // 1100
                      0,   // 1101
                      4,   // 1110
                      0    // 1111
};

//    CODES FOR REGISTRY ID
const byte REGEAX =   0;    // 000
const byte REGEBX =   1;    // 001
const byte REGECX =   2;    // 010
const byte REGEDX =   3;    // 011
const byte REGFLAGS = 4;    // 100

// *************************************************************


//Declaration of functions of instructions *********************
int setPinMode(String arguments);                // 0000 arguments: XXXX-XX -> #pin, mode 
int setPinStatus(String arguments);              // 0001 arguments: XXXX-X  -> #pin, status
int addition();                                  // 0010
int subtraction();                               // 0011
int multiplication();                            // 0100
int division();                                  // 0101
int displayLastResutl();                         // 0110
int setAXval(String arguments);                  // 0111 arguments: X-XXXXXX -> sign (0:positive, 1:negavite), value
int setBXval(String arguments);                  // 1000 arguments: X-XXXXXX -> sign (0:positive, 1:negavite), value
int showReg(String arguments);                   // 1001 arguments: XXX  -> registry
void showRegs();                                 // 1010
void setRegValue(String arguments);              // 1011 arguments:   XXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -> Registry ID, value
void getRegVal(String arguments);                // 1100 arguments:   XXX  -> Registry ID
bool compare();                                  // 1101
void readPinStatus(String arguments);            // 1110 arguments: XXXX   -> #pin
void changeMode();                               // 1111


// ************************************************************
//  Output messages FUNTIONS declaration
void wellcome();          // Weelcome message
void showHelp();          // Show help menu
void notValidF();         // Show error message for no valid function called

// ************************************************************
// Common FUNCTIONS declaration
int convertSBtoI(String str);         //Convert String of "bits" to Integer
String convertItoSB(int num);         //Convert Integer to String of "bits"
int convertChartoInt(char cara);      //Convert Character to Integer

// ************************************************************
// Registry FUNTIONS declaration
String getRegistry(boolean registry[], byte regsize, byte from, byte to);     //Get Registry's bits in a String
byte setRegistry(boolean registry[], byte regsize, byte from, String value);      // set Registry's bits 
void displayRegistry(boolean registry[], byte regsize);         //display the Registry's bits
void updateFlags(int result);               // update flags registry when an operation finished


// ************************************************************
// Control input FUNCTIONS declaration
void inputFSerial();                      // Input instruction codes from Serial
void inputFCardReader();                  // Input instruction codes from card reader machine
void execInstruction(byte ninstruction);   // execute requested instruction

// ************************************************************
// SETUP -------------
void setup() {
  Serial.begin(9600);
  pinMode(pcm_co, INPUT_PULLUP);
  pinMode(pcm_reader, INPUT_PULLUP);
  pinMode(pcm_button, INPUT_PULLUP);
  showWellcome();
}

// **************************************************************
// LOOP --------------
void loop() {

  // Setting up default initial input method (serial input, card reader input)
  inputFSerial();
  //inputFCardReader();
}

void inputFSerial(){
  imode = 0; // 0: Serial input mode stablished
  
  while(true){
  
      if(showPrompt){ // if new command listening is ready show '>' prompt
        Serial.print(F("> "));
        showPrompt = false;
      }
      
    
    if(Serial.available() > 0){ // if there is something on serial buffer
      instruction = Serial.readString();
      

      //Separate instruction bits and arguments bits
      arguments = instruction.substring(4,instruction.length());
      instruction = instruction.substring(0,4);
      
      //print read command instruction and aguments
      Serial.print(instruction);
      Serial.print(F(" "));
      Serial.println(arguments);


      // get DECIMAL number of the binary instruction code reade
      ninstruction = convertSBtoI(instruction);
  
      if(inputError == false){
        // ----------- EXECUTING INSTRUCTION -------------
        // if there was no input error
        execInstruction(ninstruction);
        
      }
      else{ //Input error, no binary entry
        if(instruction == "?" || instruction == "help"){ //if input is '?' or 'help'
          showHelp();
        }
        else if(instruction == "mode"){ // if input is 'mode' 
          changeMode();
        }
        else{ // if no binary entry or ?/help/mode command
          notValidF();
        }
        
      }
       
      
      showPrompt = true; //shows '>' for next command listening
    }
  }
}

void inputFCardReader(){
  imode = 1;  // 1: CardReader input mode stablished
  
  while(true){
    status_pcm_button = digitalRead(pcm_button);  // read cardReader machine input button

    // check if the pushbutton is pressed. If it is, the status_pcm_co is HIGH:
    // mmode 1: reading perforated card, 0: waiting for press btn to start reading perforated card
    if(mmode == 1) {
      // read the state of the pushbutton value:
      //status_pcm_button = digitalRead(pcm_button);
      //status_pcm_co = digitalRead(pcm_co);
  
  
      //initial values for each instruction statement
      byte i = 1;   // number of instruction bits read
      approbs = 0;  // number of approved signals
      instruction = ""; // final instrucion (string of bits)
  
      // READING INSTRUCTION --------------------------------------------------------
      while(i <= aiclength){
        // read continuity pin of cardReader
        status_pcm_co = digitalRead(pcm_co);
        
        
        cocounter = 0;


        while(status_pcm_co == 0){            //WHILE continuity sensor reads is on
          cocounter++;

          if(cocounter > co_margen){
            status_pcm_reader = digitalRead(pcm_reader);
            
            if(status_pcm_reader == 0){   // add +1 if cardreader Instruction pin reads 0
              appro0++;
            }
            else { // add + if cardreader instrucion pin reads 1
              appro1++;
            }
            appron++; // add +1 to total of read 
            
          }
          
          status_pcm_co = digitalRead(pcm_co);
        }
        
       
        if(appron > 0){
           if(appro0 > appro1){ // if input cardreader pin read higher number of zeros than ones.
            statusxx = 0;
            instruction += "1"; // concat an one to instruction string
          }else {
            statusxx = 1;
            instruction += "0"; // concat an zero to instruction string
          }

          //Debugging
          Serial.print(!statusxx);
          Serial.print("\t");
          Serial.print(appron);
          Serial.print("\t");
          Serial.print(appro0);
          Serial.print("\t");
          Serial.println(appro1);
          
          i++; // increment instrucion input code bits counter


          // restore counters
          //apr = 1;
          appron = 0;
          appro0 = 0;
          appro1 = 0;
          cocounter = 0;
        }
      } // END OF READ INSTRUCTION (i <= aiclength)
  
      
  
      // SHOWING READ INSTRUCTION -------------------------------------------
      Serial.print("Instruction: ");
      Serial.println(instruction);


      //Getting number of bits for arguments of read instruction
      ninstruction = convertSBtoI(instruction);
      abbio = abbix[ninstruction];
      if (ninstruction > nmaxi){
        abbio = 0;
      }
  
      // DISPLAYING Argument's # Bits OF READ INSTRUCTION ------------------
      Serial.print("Instructions Args #bits:");
      Serial.println(abbio);
      
  
      // READING ARGUMENTS Bits ---------------------------------------------
      unsigned int a = 1;
      arguments = "";

      // while number of arguments read is less than maximun for the instruccion
      while(a <= abbio){
        status_pcm_co = digitalRead(pcm_co);
        
        while(status_pcm_co == 0){            //if continuity is on
          cocounter++;

          if(cocounter > 100){
            status_pcm_reader = digitalRead(pcm_reader);
            
            if(status_pcm_reader == 0){   // add +1 if cardreader Instruction pin reads 0
              appro0++;
            }
            else { // add + if cardreader instrucion pin reads 1
              appro1++;
            }
            appron++; // add +1 to total of read 
            
          }
          
          
          
          status_pcm_co = digitalRead(pcm_co);
        }
       
        if(appron > 0){
           if(appro0 > appro1){
            statusxx = 0;
            arguments += "1";
          }else {
            statusxx = 1;
            arguments += "0";
          }
          Serial.print(!statusxx);
          Serial.print("\t");
          Serial.print(appron);
          Serial.print("\t");
          Serial.print(appro0);
          Serial.print("\t");
          Serial.println(appro1);
          
          a++;
        
          //apr = 1;
          appron = 0;
          appro0 = 0;
          appro1 = 0;
          cocounter = 0;
        }
      }
  
       // DISPLAYING READ ARGUMENTS ----------------------------------------
      Serial.print("Arguments: ");
      Serial.println(arguments);
  
  
    
  
      // ################################################
      // ----------- EXECUTING INSTRUCTION -------------
      execInstruction(ninstruction);
        
      
      
      // check if the pushbutton is pressed. If it is, the status_pcm_co is HIGH: ----
      /*if (status_pcm_co == LOW) {
        // turn LED on:
        digitalWrite(ledPin, HIGH);
      } else {
        // turn LED off:
        digitalWrite(ledPin, LOW);
      }*/
      //Serial.println(status_pcm_co); ---------------- TRYING
  
  
     mmode = 0;
    }
    
    status_pcm_button = digitalRead(pcm_button);
    if(status_pcm_button == LOW){
      mmode = 1;
      Serial.print("\n> ");
    }
    
  }
  
}

// Executing FUNCTIONS by number of funtion
void execInstruction(byte ninstruction){
  switch(ninstruction){
        case SETPINMODE: {
          setPinMode(arguments);
          break;
        }
        case SETPINSTATUS: {
          setPinStatus(arguments);
          break;
        }
        case ADDITION: {
          addition();
          break;
        }
        case SUBSTRACTION: {
          subtraction();
          break;
        }
        case MULTIPLICATION: {
          multiplication();
          break;
        }
        case DIVISION: {
          division();
          break;
        }
        case DISPLAYLASTRESULT: {
          displayLastResult();
          break;
        }
        case SETAXVAL: {
          setAXval(arguments);
          break;
        }
        case SETBXVAL: {
          setBXval(arguments);
          break;
        }
        case SHOWREG: {
          showReg(arguments);
          break;
        }
        case SHOWREGS:{
          showRegs();
          break;
        }
        case SETREGVALUE:{
          setRegValue(arguments);
          break;
        }
        case GETREGVALUE: {
          getRegValue(arguments);
          break;
        }
        case COMPARE: {
          compare();
          break;
        }
        case READPINSTATUS: {
          readPinStatus(arguments);
          break;
        }
        case CHANGEMODE:  {
          changeMode();
          break;
        }
        default: {
          notValidF();
          break;
        }
      }
}



// ********************************************************************************

// DEFINITION OF FUNCTIONS OF OPERATIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Function of instruction 0000
int setPinMode(String arguments){
  // getting arguments
  String spin = arguments.substring(0,4);  //get arguments bits from 0 to 4 (pin)
  String smode = arguments.substring(4,6); //get arguments' bits from 4 to 6 (mode)

  // converting strings to int (arguments)
  int pin = convertSBtoI(spin);
  int mode = convertSBtoI(smode);

  // displaying executing command
      Serial.print(F("setPinMode("));
      Serial.print(pin);
      
  switch(mode){
    case 0: {
      // executing instruction 
      pinMode(pin, INPUT);

      
      Serial.println(F(", INPUT);"));
      break;
    }
    case 1: {
      // executing instruction
      pinMode(pin, INPUT_PULLUP);

      // displaying executing command
      Serial.println(F(", INPUT_PULLUP);"));
      break;
    }
    case 2: {
      // executing instruction
      //pinMode(pin, INPUT_PULLDOWN);
      break;
    }
    case 3: {
      // executing instruction
      pinMode(pin, OUTPUT);

      // displaying executing command
      Serial.println(F(", OUTPUT);"));
      break;
    }

    default: {
      break;
    }
      
  }

  return 0;
}

// Function of instruction 0001
int setPinStatus(String arguments){
  // getting arguments
  String spin = arguments.substring(0,4);
  String sstatus = arguments.substring(4,5);

  //converting strings to int (Arguments)
  int pin = convertSBtoI(spin);
  int status = convertSBtoI(sstatus);
  
  
  // ------- Displaying Instruction command
  Serial.print(F("setPinStatus("));
  Serial.print(pin);
  Serial.print(F(", "));
  Serial.print(status);
  Serial.println(F(")"));

  // -------- Executing instruction
  digitalWrite(pin, status);

  
  return 0;
}

// Function of instruction 0010
int addition(){
  // Executing instruction

  if(getRegistry(EAX,sizeof(EAX),0,0) == "0"){
    AX = convertSBtoI(getRegistry(EAX,sizeof(EAX),16,31));
  }else{
    AX = -1 * convertSBtoI(getRegistry(EAX,sizeof(EAX),16,31));
  }

  if (getRegistry(EBX,sizeof(EBX),0,0) == "0"){
    BX = convertSBtoI(getRegistry(EBX,sizeof(EBX),16,31));
  }else{
    BX = -1 * convertSBtoI(getRegistry(EBX,sizeof(EBX),16,31));
  }
  
  //                     get registry EAX(32 bits)[from 16 to 31 (i.e AX)] and convert it to integer
  Serial.println(AX);
  Serial.println(BX);
  LRESULT = AX + BX;

  // Clean EDX Registry
  cleanRegistry(EDX,sizeof(EDX));


  //converting result to string of bits
  String StringBit = convertItoSB(abs(LRESULT));

  //saving result to registry DX
  setRegistry(EDX, sizeof(EDX), (sizeof(EDX)-StringBit.length()), StringBit);
  if (LRESULT>= 0){
    setRegistry(EDX, sizeof(EDX), 0, "0000000000000000");
  }else{
    //Serial.print(F("-"));
    setRegistry(EDX, sizeof(EDX), 0, "1111111111111111");
  }


  updateFlags(LRESULT);
  

  
  // Displaying Executing command
  Serial.println(F("Addition(AX+BX)"));
  
  return 0;
}

// Function of instruction 0011
int subtraction(){
  // Executing instruction
  

  if (getRegistry(EBX,sizeof(EBX),0,0) == "0"){
    BX = convertSBtoI(getRegistry(EBX,sizeof(EBX),16,31));
  }else{
    BX = -1 * convertSBtoI(getRegistry(EBX,sizeof(EBX),16,31));
  }
  
  //                     get registry EAX(32 bits)[from 16 to 31 (i.e AX)] and convert it to integer
  LRESULT = AX - BX;

  // Clean EDX Registry
  cleanRegistry(EDX,sizeof(EDX));

  
  //converting result to string of bits
  String StringBit = convertItoSB(abs(LRESULT));

  //saving result to registry DX
  setRegistry(EDX, sizeof(EDX), (sizeof(EDX)-StringBit.length()), StringBit);
  if (LRESULT>= 0){
    setRegistry(EDX, sizeof(EDX), 0, "0000000000000000");
  }else{
    //Serial.print(F("-"));
    setRegistry(EDX, sizeof(EDX), 0, "1111111111111111");
  }

  
  updateFlags(LRESULT);

  // Displaying Executing command
  Serial.println(F("Subtraction(AX-BX)"));
  
  return 0;
}

// Function of instruction 0100
int multiplication(){
  // Executing instruction
  //                     get registry EAX(32 bits)[from 16 to 31 (i.e AX)] and convert it to integer
  LRESULT = convertSBtoI(getRegistry(EAX,sizeof(EAX),16,31)) * convertSBtoI(getRegistry(EBX,sizeof(EBX),16,31));

  
  // Clean EDX Registry
  cleanRegistry(EDX,sizeof(EDX));

  
  //Converting result(int) to string of bits
  String StringBit = convertItoSB(LRESULT);

  // Saving result to DX registry
  setRegistry(EDX, sizeof(EDX), (sizeof(EDX)-StringBit.length()), StringBit);

  String zeax = getRegistry(EAX,sizeof(EAX),0,0);
  String zebx = getRegistry(EBX,sizeof(EBX),0,0);
  if((zeax == "0" && zebx == "0") || (zeax == "1" && zebx == "1")){
    setRegistry(EDX, sizeof(EDX), 0, "0000000000000000");
  }else{
    setRegistry(EDX, sizeof(EDX), 0, "1111111111111111");
    LRESULT *= -1;
  }



  //Setting FLAGS $$$$$$$$$$$$$$$$$$$$$$
  //If Result is negative, turn on SF
  updateFlags(LRESULT);

  // Displaying Executing command
  Serial.println(F("Multiplication(AX*BX)"));
  
  return 0;
}

// Function of instruction 0101
int division(){
  // Executing instruction
  //                     get registry EAX(32 bits)[from 16 to 31 (i.e AX)] and convert it to integer
  LRESULT = convertSBtoI(getRegistry(EAX,sizeof(EAX),16,31)) / convertSBtoI(getRegistry(EBX,32,16,31));


  // Clean EDX Registry
  cleanRegistry(EDX,sizeof(EDX));

  
  //Converting result(int) to string of bits
  String StringBit = convertItoSB(LRESULT);

  // Saving result to DX registry
  setRegistry(EDX, sizeof(EAX), (sizeof(EAX)-StringBit.length()), StringBit);

  String zeax = getRegistry(EAX,sizeof(EAX),0,0);
  String zebx = getRegistry(EBX,sizeof(EBX),0,0);
  if((zeax == "0" && zebx == "0") || (zeax == "1" && zebx == "1")){
    setRegistry(EDX, sizeof(EDX), 0, "0000000000000000");
  }else{
    setRegistry(EDX, sizeof(EDX), 0, "1111111111111111");
    LRESULT *= -1;
  }



  //Setting FLAGS $$$$$$$$$$$$$$$$$$$$$$
  updateFlags(LRESULT);

  // Displaying Executing command
  Serial.println(F("Division(AX/BX)"));
  
  return 0;
}

// Function of instruction 0110
int displayLastResult(){
  // Executing instruction
  Serial.println(F("displayLastResult()"));
  Serial.print(F("\t= "));

  if (LRESULT<0){
    Serial.print(F("-"));
  }
  int result = convertSBtoI(getRegistry(EDX, sizeof(EDX), 16, 31));
  Serial.println(result);

  return 0;
}

// Function of instruction 0111
int setAXval(String arguments){
  // spliting sign and values
  String sign = arguments.substring(0,1);
  String value = arguments.substring(1,arguments.length());
  
  //converting argument string to int
  AX = convertSBtoI(value);
  Serial.print(F("AX: "));

  //knowing arguments' length
  byte aleng = value.length();

  //if first bit of argument is 0, set left 16 bits of BX as 0s, to represent positive number
  // else set bits to 1s, to represent negative numbers
  if (sign == "0"){
    setRegistry(EAX, sizeof(EAX), 0, "0000000000000000");
  }else{
    Serial.print(F("-"));
    setRegistry(EAX, sizeof(EAX), 0, "1111111111111111");
  }
  
  //Saving the value to registry AX (EAX(16,31)
  setRegistry(EAX, sizeof(EAX), sizeof(EAX)-aleng, value);
  
  // Displaying executing value
  Serial.println(AX);
  
  return 0;
}

// Function of instruction 1000
int setBXval(String arguments){
  // spliting sign and values
  String sign = arguments.substring(0,1);
  String value = arguments.substring(1,arguments.length());

  
  //converting argument string to int
  BX = convertSBtoI(value);
  Serial.print(F("BX: "));

  //knowing arguments' length
  byte aleng = value.length();

  //if first bit of argument is 0, set left 16 bits of BX as 0s, to represent positive number
  // else set bits to 1s, to represent negative numbers
  if (sign == "0"){
    setRegistry(EBX, sizeof(EBX), 0, "0000000000000000");
  }else{
    Serial.print(F("-"));
    setRegistry(EBX, sizeof(EBX), 0, "1111111111111111");
  }
  
  //Saving the value to registry AX (EAX(16,31)
  setRegistry(EBX, sizeof(EBX), sizeof(EBX)-aleng, value);
  
  // Displaying executing value
  Serial.println(BX);
  
  return 0;
}

// Function of instruction 1001
int showReg(String arguments){
  int narg = convertSBtoI(arguments);

  /*
   * ShowReg Function
   * Arguments: XXX -> Registry ID to show
   *    000: EAX Registry
   *    001: EBX Registry
   *    010: ECX Registry
   *    011: EDX Registry
   *    100: FLAGS Registry
   */

  switch(narg){
    case REGEAX:{
      Serial.print(F("EAX: "));
      displayRegistry(EAX, sizeof(EAX));
      break;
    }
    case REGEBX:{
      Serial.print(F("EBX: "));
      displayRegistry(EBX, sizeof(EBX));
      break;
    }
    case REGECX:{
      Serial.print(F("ECX: "));
      displayRegistry(ECX, sizeof(ECX));
      break;
    }
    case REGEDX:{
      Serial.print(F("EDX: "));
      displayRegistry(EDX, sizeof(EDX));
      break;
    }
    case REGFLAGS:{
      Serial.print(F("FLAGS: "));
      displayRegistry(FLAGS, sizeof(FLAGS));
      Serial.println(F("        CF 1  PF 0  AF 0  ZF SF TF IF DF OF -  -  -  -"));
      break;
    }

    default:
      break;
  }
}

// FUNTION for 1010
void showRegs(){
      Serial.print(F("EAX: "));
      displayRegistry(EAX, sizeof(EAX));
      Serial.print(F("EBX: "));
      displayRegistry(EBX, sizeof(EBX));
      Serial.print(F("ECX: "));
      displayRegistry(ECX, sizeof(ECX));
      Serial.print(F("EDX: "));
      displayRegistry(EDX, sizeof(EDX));
      Serial.print(F("FLAGS: "));
      displayRegistry(FLAGS, sizeof(FLAGS));
      Serial.println(F("        CF 1  PF 0  AF 0  ZF SF TF IF DF OF -  -  -  -"));
}

// Function for 1011
void setRegValue(String arg){
  int narg = convertSBtoI(arg.substring(0,3));
  arg = arg.substring(3,arg.length());

  /*
   * ShowReg Function
   * Arguments: XXX -> Registry ID to show
   *    000: EAX Registry
   *    001: EBX Registry
   *    010: ECX Registry
   *    011: EDX Registry
   *    100: FLAGS Registry
   */

  switch(narg){
    case REGEAX:{
      Serial.print(F("SetRegValue(EAX) "));
        
        //knowing arguments' length
        byte aleng = arg.length();

        //Saving the argument(number) to registry EAX
        setRegistry(EAX, sizeof(EAX), sizeof(EAX)-aleng, arg);
        Serial.println(F("EAX: "));
        displayRegistry(EAX, sizeof(EAX));
      
      break;
    }
    case REGEBX:{
      Serial.println(F("SetRegValue(EBX) "));
        
        //knowing arguments' length
        byte aleng = arg.length();

        //Saving the argument(number) to registry EBX
        setRegistry(EBX, sizeof(EBX), sizeof(EBX)-aleng, arg);
        Serial.println(F("EBX: "));
        displayRegistry(EBX, sizeof(EBX));
      
      break;
    }
    case REGECX:{
      Serial.print(F("SetRegValue(ECX) "));
        
        //knowing arguments' length
        byte aleng = arg.length();

        //Saving the argument(number) to registry ECX
        setRegistry(ECX, sizeof(ECX), sizeof(ECX)-aleng, arg);
        Serial.println(F("ECX: "));
        displayRegistry(ECX, sizeof(ECX));
      
      break;
    }
    case REGEDX:{
      Serial.print(F("SetRegValue(EDX) "));
        
        //knowing arguments' length
        byte aleng = arg.length();

        //Saving the argument(number) to registry EAX
        setRegistry(EDX, sizeof(EDX), sizeof(EDX)-aleng, arg);
        Serial.println(F("EDX: "));
        displayRegistry(EDX, sizeof(EDX));
      
      break;
    }
    case REGFLAGS:{
      Serial.print(F("SetRegValue(FLAGS) "));
        
        //knowing arguments' length
        byte aleng = arg.length();

        //Saving the argument(number) to registry FLAGS
        setRegistry(FLAGS, sizeof(FLAGS), sizeof(FLAGS)-aleng, arg);
        Serial.println(F("FLAGS: "));
        displayRegistry(FLAGS, sizeof(FLAGS));
        Serial.println(F("        CF 1  PF 0  AF 0  ZF SF TF IF DF OF -  -  -  -"));
      
      break;
    }

    default:
      break;
  }
}

// Funtion for 1100 - get registry value casted to Decimal
void getRegValue(String arg){
  int narg = convertSBtoI(arguments);

  /*
   * ShowReg Function
   * Arguments: XXX -> Registry ID to show
   *    000: EAX Registry
   *    001: EBX Registry
   *    010: ECX Registry
   *    011: EDX Registry
   *    100: FLAGS Registry
   */

  switch(narg){
    case REGEAX:{
      Serial.print(F("Dec-Val(EAX): "));
      int result = convertSBtoI(getRegistry(EAX, sizeof(EAX), 16, 31));
      if(convertSBtoI(getRegistry(EAX,sizeof(EAX),0,0)) == 1){// if first element is 1, means decimal 
        Serial.print(F("-"));
      }
      Serial.println(result);
      break;
    }
    case REGEBX:{
      Serial.print(F("Dec-Val(EBX): "));
      int result = convertSBtoI(getRegistry(EBX, sizeof(EBX), 16, 31));
      if(convertSBtoI(getRegistry(EBX,sizeof(EBX),0,0)) == 1){ // if first element is 1, means decimal 
        Serial.print(F("-"));                         //            number is negative
      }
      Serial.println(result);
      break;
    }
    case REGECX:{
      Serial.print(F("Dec-Val(ECX): "));
      int result = convertSBtoI(getRegistry(ECX, sizeof(ECX), 16, 31));
      if(convertSBtoI(getRegistry(ECX,sizeof(ECX),0,0)) == 1){ // if first element is 1, means decimal 
        Serial.print(F("-"));                         //            number is negative
      }
      Serial.println(result);
      break;
    }
    case REGEDX:{
      Serial.print(F("Dec-Val(EDX): "));
      int result = convertSBtoI(getRegistry(EDX, sizeof(EDX), 16, 31));
      if(convertSBtoI(getRegistry(EDX,sizeof(EDX),0,0)) == 1){ // if first element is 1, means decimal 
        Serial.print(F("-"));                         //            number is negative
      }
      Serial.println(result);
      break;
    }
    case REGFLAGS:{
      Serial.print(F("Dec-Val(FLAGS): "));
      int result = convertSBtoI(getRegistry(FLAGS, sizeof(FLAGS), 0, 15));
      Serial.println(result);
      break;
    }

    default:
      break;
  }
}

// Function for 1101 code Compare EAX adn EBX registries
bool compare(){
  // Return  <-  0: Equals, 1: Different    (registry values)
  
  Serial.println(F("COMPARE(EAX, EBX)"));
  
  for(byte i=0; i < 32; i++){
    if(EAX[i] != EBX[i]){ // If EAX element is different to EBX element
      setRegistry(EDX,sizeof(EDX),31,"1"); // set EDX[31] element to ONE
      Serial.println(F("\t= 1"));
      return 1;
    }  
  }

  // If whole EAX elemets are equeals to EBX elements
  Serial.println(F("\t= 0")); 
  setRegistry(EDX,sizeof(EDX),31,"0"); // set EDX[31] elemente to ZERO
  return 0;
}

// Funtion of 1110 instruction code, read status from a specified pin
void readPinStatus(String arguments){
  // getting arguments
  String spin = arguments.substring(0,4);  //get arguments bits from 0 to 4 (pin)

  // converting strings to int (arguments)
  int pin = convertSBtoI(spin);

  bool status = digitalRead(pin);  // Only digital pins yet
  Serial.print("\t=");
  Serial.println(status);

  return;

}

// Change input mode (Serial to Cardreader and Viseversa)
void changeMode(){
  if(imode == 0){ // if currently is Serial input mode 
    Serial.println(F("CardReader mode established"));
    inputFCardReader();
  }else{ // if currently is CardReader input mode
    Serial.println(F("Serial mode established"));
    inputFSerial();
  }
}

// Error message of no valid function called
void notValidF(){
  Serial.println(F("Error: No valid instruction code entered. type 'help' or '?' for help"));

  return;
}




// MESSAGES FUNCTIONS DEFINITION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Show help text menu
void showHelp(){
  Serial.println(F("Help -------------------------------------"));
  Serial.println(F("All entry commands should be entered on binary code (except help)"));
  Serial.println(F("IMPORTANT: The entire command must be together without spaces."));
  Serial.println(F("The commads have 4bits of instruction code (first left-side bits)"));
  Serial.println(F("  but some some commands expect more bits for arguments (right-side bits)"));
  
  Serial.println(F(" Example:"));
  Serial.println(F("    0111000010         first 4 bits -> instruction code: setAXRegistryValue()"));
  Serial.println(F("                       last 6 bits -> argument value: number 2 (on binary code)"));
  Serial.println(F(" So in this example, it will execute the operation: set number 2 (on binary code) to AX Registry"));
  Serial.println(F(""));
  Serial.println(F("You can use any of the following binary entries:"));
  Serial.println(F(" 0000: setPinMode    arguments: XXXX-XX  -> #I/O pin binary(2-13), Mode (00:INPUT, 11:OUTPUT)"));
  Serial.println(F(" 0001: setPinStatus  arguments: XXXX-X   -> #I/O pin binary(2-13), Status (0:LOW, 1:HIGH)"));
  Serial.println(F(" 0010: addition"));
  Serial.println(F(" 0011: subtraction"));
  Serial.println(F(" 0100: multiplication"));
  Serial.println(F(" 0101: division"));
  Serial.println(F(" 0110: displayLastResult (of the last A/L operation)"));
  Serial.println(F(" 0111: setAXval      arguments: X-XXXXX -> sign (0: +, 1: -), value(Binary code of an integer value)"));
  Serial.println(F(" 1000: setBXval      arguments: X-XXXXX -> sign (0: +, 1: -), value(Binary code of an integer value)"));
  Serial.println(F(" 1001: showRegistry  arguments: XXX -> 000:EAX, 001:EBX, 010:ECX, 011:EDX, 100:FLAGS"));
  Serial.println(F(" 1010: showAllRegistry (EAX, EBX, ECX, EDX, FLAGS)"));
  Serial.println(F(" 1011: setRegValue   arguments: XXX-X(16-32) -> 000:EAX, 001:EBX, 010:ECX, 011:EDX, 100:FLAGS"));
  Serial.println(F(" 1100: getRegValue   arguments: XXX -> 000:EAX, 001:EBX, 010:ECX, 011:EDX, 100:FLAGS"));
  Serial.println(F(" 1101: compare       (compare EAX and EBX bits) (result 0: equal, 1: different)"));
  Serial.println(F(" 1110: readPinStatus  arguments: XXXX   -> #I/O pin binary(2-13)"));
  Serial.println(F(" 1111: changeMode  change mode (to SerialInput or CardReaderInput)"));
  Serial.println(F(""));
  Serial.println(F("    @ You can change input mode to CardReader Machine typing 'mode'"));
  
  return;
}

// Show wellcome text 
void showWellcome(){
  Serial.println(F(" ██████╗  ███████╗████████╗██████╗   ██████╗                 ██████╗     ██████╗                    "));
  Serial.println(F(" ██╔══██╗██╔════╝╚══██╔══╝██╔══██╗██╔═══██╗               ██╔══██╗  ██╔════╝                    "));
  Serial.println(F(" ██████╔╝█████╗         ██║     ██████╔╝██║     ██║ █████╗    ██████╔╝ ██║                         "));
  Serial.println(F(" ██╔══██╗██╔══╝         ██║     ██╔══██╗██║     ██║ ╚════╝    ██╔═══╝   ██║                         "));
  Serial.println(F(" ██║  ██║ ███████╗      ██║     ██║  ██║╚██████╔╝                 ██║          ╚██████╗                    "));
  Serial.println(F(" ╚═╝  ╚═╝╚══════╝       ╚═╝     ╚═╝  ╚═╝ ╚═════╝                   ╚═╝            ╚═════╝                    "));
  Serial.println(F("           ███████╗██╗███╗     ███╗██╗   ██╗  ██╗          █████╗ ████████╗  ██████╗   ██████╗ "));
  Serial.println(F("           ██╔════╝██║████╗  ████║██║   ██║  ██║        ██╔══██╗╚══██╔══╝██╔═══██╗██╔══██╗"));
  Serial.println(F("           ███████╗██║██╔████╔██║██║   ██║  ██║        ███████║      ██║     ██║     ██║██████╔╝"));
  Serial.println(F("           ╚════██║██║██║╚██╔╝██║██║   ██║  ██║        ██╔══██║      ██║     ██║     ██║██╔══██╗"));
  Serial.println(F("           ███████║██║██║ ╚═╝  ██║╚██████╔╝███████╗██║   ██║      ██║     ╚██████╔╝██║    ██║"));
  Serial.println(F("           ╚══════╝╚═╝╚═╝        ╚═╝ ╚═════╝   ╚══════╝╚═╝  ╚═╝      ╚═╝      ╚═════╝   ╚═╝    ╚═╝"));
  Serial.println(F("Wellcome to RetroPC-Simulator"));
  Serial.println(F("Developed by: Luis Quijada"));
  Serial.println(F("Email: jose.jlq@hotmail.com"));
  Serial.println(F(""));
  
  Serial.println(F("Type 'help' or '?' for help and information"));
  Serial.println(F("Starting..."));
}




// COMMUN / GENERAL FUNCTIONS DEFINITION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// convert binary string to decimal int 
int convertSBtoI(String str){
  inputError=false;
  int number = 0;
  int o = 1;

  //desde el (final-1) del string hasta 0
  for(int i = str.length() -1; i >= 0; i--){
    if(str[i] == '1'){  // if the bit is 1
      number += o;      // numero = numero * o (o=something powered to 2)
    }

    o = o * 2; // to make it in serie 2, 4, 8...
    if(str[i] != '1' && str[i] != '0'){
      inputError=true;
    }
  }

  return number;
}

// Convert Int to Stringbit (String with zeros and ones)
String convertItoSB(int num){
  String SB = "";
  
  while (num / 2 != 0) {  //while there are dividens
    SB = (num % 2) + SB;  // 
    num = num / 2;
  }
  SB = (num % 2) + SB;

  return SB;
}



// REGISTRY FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//Get registry value from an index
String getRegistry(boolean registry[], byte regsize, byte from, byte to){
  String snumber = "";

  //checking if final interval is not higher than registry' size
  if(to <= (regsize - 1)) {
    for(byte i = from; i<=to;i++){
      snumber += registry[i];
    }
  } else {
    Serial.println(F("Error: getting data on registry, Memory overflow"));
  }
  
  return snumber;
}

//Set registry value from an index
byte setRegistry(boolean registry[], byte regsize, byte from, String value){
  byte i = from;
  String aa;
  //byte nelements=sizeof(registry)/sizeof(registry[0]);

   //checking if final interval is not higher than registry' size
  if((value.length()+from-1) <= (regsize - 1)) {
    for (byte s=0; s<value.length(); s++){
      aa = String(value[s]);
      registry[i] = aa.toInt();
      i++;
    }
  } else {
    Serial.println(F("Error: setting data on registry, Memory overflow"));
  }
  


  return 0;
}

// Display Registry as a vector
void displayRegistry(boolean registry[], byte regsize){
  Serial.print(F("["));
  for(byte i=0; i<regsize; i++){
    Serial.print(registry[i]);
    Serial.print(F("  "));
  }
  Serial.println(F("]"));
}

// Return parity of a Registry (0: Even number of ones, 1: Odd number of ones)
boolean getRegParity(boolean registry[]){
  byte total_ones = 0;

  for(byte i=16;i< genreg_length; i++){
    if(registry[i] == 1){total_ones++;}
  }

  //Serial.println(total_ones);
  //Serial.println(total_ones%2);

  return (total_ones%2);
}

// Funtion to update flags registry after and Logic/Arithmetic operation
void updateFlags(int result){
  //   Setting UP FLAGS 

  
  //If Result is negative, turn on SF
  if(LRESULT < 0){
    setRegistry(FLAGS, sizeof(FLAGS), 7, "1");
  } else {
    setRegistry(FLAGS, sizeof(FLAGS), 7, "0");
  }
  
  //If Result is zero, turn on ZF
  if(LRESULT == 0){
    setRegistry(FLAGS, sizeof(FLAGS), 6, "1");
  }else {
    setRegistry(FLAGS, sizeof(FLAGS), 6, "0");
  }
  
  //If result is pair, turn on PF
  if(getRegParity(EDX) == 0){ //si es par
    setRegistry(FLAGS, sizeof(FLAGS), 2, "1");
  }else{//si no es par
    setRegistry(FLAGS, sizeof(FLAGS), 2, "0");
  }

  //if result is higher than 32768 (heigher representable number)
  // turn on CF 
  if(result > 32768 || result < -32768){
    setRegistry(FLAGS, sizeof(FLAGS), 0, "1");
  }else{
    setRegistry(FLAGS, sizeof(FLAGS), 0 , "0");
  }
}


// Clean Registry (set zeros to all values)
void cleanRegistry(boolean registry[], byte regsize){
  for(byte i=0; i< regsize;i++){
    registry[i] = 0;
  }
}

